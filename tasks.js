'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
 */

function firstTask() {
  const menuBtn = document.getElementById('menuBtn');
  const menu = document.getElementById('menu');
  menuBtn.addEventListener('click', openMenu);

  const menuList = document.querySelectorAll('.menu-item');
  for (const item of menuList) {
    item.addEventListener('click', closeMenu);
  }
  function closeMenu() {
    menu.style.display = 'none';
    console.log('closed');
    menuBtn.removeEventListener('click', closeMenu);
    menuBtn.addEventListener('click', openMenu);
  }
  function openMenu() {
    menu.style.display = 'block';
    console.log('opened');
    menuBtn.removeEventListener('click', openMenu);
    menuBtn.addEventListener('click', closeMenu);
  }
}

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 */

function secondTask() {
  const block = document.getElementById('movedBlock');
  block.style.transition = `all 0.2s ease-out`;
  const blockBounding = block.getBoundingClientRect();
  const rect = document.getElementById('field').getBoundingClientRect();
  field.addEventListener('click', (event) => {
    if (event.clientX > rect.x && event.clientX < rect.width + rect.x) {
      if (event.clientY > rect.y && event.clientY < rect.height + rect.y) {
        let translateX = event.clientX - rect.left;
        let translateY = event.clientY - rect.top;
        if (event.clientX - rect.x > rect.width - blockBounding.width) {
          translateX = 450;
        }
        if (event.clientY - rect.y > rect.height - blockBounding.height) {
          translateY = 250;
        }
        block.style.transform = `translate(${translateX}px, ${translateY}px)`;
      }
    }
  });
}

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 */
function thirdTask() {
  document.getElementById('messager').onclick = function (event) {
    const { target } = event;
    if (target.classList.contains('remove')) {
      target.parentNode.remove();
    }
  };
}
/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
 */
function fourthTask() {
  document.getElementById('links').addEventListener('click', (event) => {
    const { target } = event;
    if (target.hasAttribute('href')) {
      event.preventDefault(); //отменяем дефолтное поведение, иначе несмотря на фолс происходит редирект
      const answer = confirm('Вы точно желаете покинуть страницу?');
      if (answer) {
        window.location = target.getAttribute('href');
      }
    }
  });
}
/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
 */
function fifthTask() {
  document.getElementById('fieldHeader').addEventListener('input', (event) => {
    document.getElementById('taskHeader').innerHTML = event.currentTarget.value;
  });
}

firstTask();
secondTask();
thirdTask();
fourthTask();
fifthTask();
